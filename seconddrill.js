let data = [{"id":1,"first_name":"Valera","last_name":"Pinsent","email":"vpinsent0@google.co.jp","gender":"Male","ip_address":"253.171.63.171"},
{"id":2,"first_name":"Kenneth","last_name":"Hinemoor","email":"khinemoor1@yellowbook.com","gender":"Polygender","ip_address":"50.231.58.150"},
{"id":3,"first_name":"Roman","last_name":"Sedcole","email":"rsedcole2@addtoany.com","gender":"Genderqueer","ip_address":"236.52.184.83"},
{"id":4,"first_name":"Lind","last_name":"Ladyman","email":"lladyman3@wordpress.org","gender":"Male","ip_address":"118.12.213.144"},
{"id":5,"first_name":"Jocelyne","last_name":"Casse","email":"jcasse4@ehow.com","gender":"Agender","ip_address":"176.202.254.113"},
{"id":6,"first_name":"Stafford","last_name":"Dandy","email":"sdandy5@exblog.jp","gender":"Female","ip_address":"111.139.161.143"},
{"id":7,"first_name":"Jeramey","last_name":"Sweetsur","email":"jsweetsur6@youtube.com","gender":"Genderqueer","ip_address":"196.247.246.106"},
{"id":8,"first_name":"Anna-diane","last_name":"Wingar","email":"awingar7@auda.org.au","gender":"Agender","ip_address":"148.229.65.98"},
{"id":9,"first_name":"Cherianne","last_name":"Rantoul","email":"crantoul8@craigslist.org","gender":"Genderfluid","ip_address":"141.40.134.234"},
{"id":10,"first_name":"Nico","last_name":"Dunstall","email":"ndunstall9@technorati.com","gender":"Female","ip_address":"37.12.213.144"}]



// 1. Find all people who are Agender

let agender = data.filter((value)=>{
    if(value.gender==='Agender')
    {
        return value;
    }
})
console.log(agender)
// 2. Split their IP address into their components eg. 111.139.161.143 has components [111, 139, 161, 143]
let sliptProblem = data.map(slipp=>{
    let sliptting = slipp.ip_address.split('.')
    let ipinnum = sliptting.reduce((accumulator, current) => {
            accumulator.push(parseInt(current));
            return accumulator;
        }, []);
        sliptting.ip_address = ipinnum
        return sliptting.ip_address;
    })
console.log(sliptProblem)
// 3. Find the sum of all the second components of the ip addresses.
let sum = sliptProblem.reduce((accumulator,current)=>{
   return accumulator += current[1]
} ,0)
console.log(sum)
// 3. Find the sum of all the fourth components of the ip addresses.
let secondsum = sliptProblem.reduce((accumulator,current)=>{
    return accumulator += current[3]
 } ,0)
 console.log(secondsum)

// 4. Compute the full name of each person and store it in a new key (full_name or something) for each person.
let fullname = data.map(x=>{
    let nename = x.first_name+" "+x.last_name
    return nename
})
console.log(fullname)
// 5. Filter out all the .org emails
let filterdata = data.filter(valueof=>{
    if(valueof.email.endsWith('.org')){
        return valueof
    }
})
console.log(filterdata)
// 6. Calculate how many .org, .au, .com emails are there
let countingdata = data.reduce((accumulator,currentvalue)=>{
    if(currentvalue.email.endsWith('.org'))
    {
        if(accumulator['.org']){
            accumulator['.org']++
        }
        else{
            accumulator['.org'] = 1
        }
    }
    if(currentvalue.email.endsWith('.au'))
    {
        if(accumulator['.au']){
            accumulator['.au']++
        }
        else{
            accumulator['.au'] = 1
        }
    }if(currentvalue.email.endsWith('.com'))
    {
        if(accumulator['.com']){
            accumulator['.com']++
        }
        else{
            accumulator['.com'] = 1
        }
    }
    return accumulator
},{})
console.log(countingdata)
// 7. Sort the data in descending order of first name
let sortdata = data.sort((a,b)=>a.first_name>b.first_name ? -1 : 1) 
console.log(sortdata)